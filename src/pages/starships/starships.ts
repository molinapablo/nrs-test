import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, LoadingController } from "ionic-angular";
import { StarshipsProvider } from "../../providers/index.services";

@Component({
  selector: "page-starships",
  templateUrl: "starships.html"
})
export class StarshipsPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public starshipsProvider: StarshipsProvider,
    public loadingCtrl: LoadingController
  ) {

  }


  next(infiniteScroll){
    this.starshipsProvider.getAllStarships()
          .then( ()=>{
            infiniteScroll.complete();
          });
  }

}
