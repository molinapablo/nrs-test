import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { VehiclesProvider } from '../../providers/index.services';

@Component({
  selector: 'page-vehicles',
  templateUrl: 'vehicles.html',
})
export class VehiclesPage {

  loading:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public vehiclesProviders: VehiclesProvider) {
  }


  next(infiniteScroll){
    this.vehiclesProviders.getAllVehicles()
          .then( ()=>{
            infiniteScroll.complete();
          });
  }


}
