export { PeopleDetailPage } from './people-detail/people-detail';
export { PeoplePage } from './people/people';
export { StarshipsPage } from './starships/starships';
export { VehiclesPage } from './vehicles/vehicles';
export { FilmsPage } from './films/films';