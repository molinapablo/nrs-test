import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, LoadingController } from "ionic-angular";
import { FilmsProvider} from "../../providers/index.services";
import { People } from '../../providers/people/people';
import { PeoplePage } from '../people/people';

@Component({
  selector: "page-films",
  templateUrl: "films.html"
})
export class FilmsPage {

  people: People[] = [];
  loading:any;
  films:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public filmsProvider: FilmsProvider,
    public loadingCtrl: LoadingController
  ) {
    this.presentLoadingDefault();
    
  }

  ionViewDidLoad()
  {
    this.filmsProvider.getAllFilms().then(data => {
      console.log(data);
      this.films=data;
      console.log(data);
      this.loading.dismiss();
    }).catch(
      function(e) {
        console.log('Error: ', e);
        throw e;}
    );
  }

  presentLoadingDefault() {

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    this.loading.present();
  
  }

  itemSelected(film) {
    
    this.navCtrl.push(PeoplePage, { title: film.title, people: film.characters});

  }
}
