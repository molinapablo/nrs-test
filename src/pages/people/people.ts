import { Component } from "@angular/core";
import { NavController, NavParams, LoadingController, ModalController } from "ionic-angular";
import { FilmsProvider, PeopleProvider } from "../../providers/index.services";
import { PeopleDetailPage } from '../people-detail/people-detail';

@Component({
  selector: "page-people",
  templateUrl: "people.html"
})
export class PeoplePage {

  filmname: string;
  characters:string[];
  loading:any;
  people:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public filmsProvider: FilmsProvider,
    public loadingCtrl: LoadingController,
    public peopleProvider: PeopleProvider,
    private modalCtrl:ModalController
  ) {

    this.presentLoadingDefault();
    this.filmname = this.navParams.get('title');
    this.characters =this.navParams.get('people');

  }

  ionViewDidLoad(){
    this.peopleProvider.getPeopleByFilm(this.characters).then(data => {
      this.people= data;
      console.log(this.people);
      this.loading.dismiss();
    }).catch(
      function(e) {
        console.log('Error: ', e);
        throw e;}
    );
  }

  itemSelected(item) {
    let profileModal = this.modalCtrl.create(PeopleDetailPage, { person: item });
    profileModal.present();
  }

  presentLoadingDefault() {

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    this.loading.present();
  
  }
}
