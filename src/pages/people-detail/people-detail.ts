import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController } from 'ionic-angular';

@Component({
  selector: 'page-people-detail',
  templateUrl: 'people-detail.html',
})
export class PeopleDetailPage {

  person:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.person =this.navParams.get('person');
    console.log("On Page Detail");
    console.log(this.person);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
