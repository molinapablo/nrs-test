import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { URL_SWAPI } from "../../config/config.services";
import { Observable } from "rxjs/Observable";

@Injectable()
export class FilmsProvider {

  constructor(public http: HttpClient) {
    console.log("Hello FilmsProvider Provider");
  }

  getAllFilms() {
    
    let promise = new Promise((resolve, reject) => {
      let url = URL_SWAPI + "films";
      this.http
        .get(url)
        .map(response => response)
        .subscribe(data => {
          console.log(data);
          if (data['error']) {
          } else {
            resolve(data['results']);
          }
        },e => {console.error(e);});
    });

    return promise;
  }
}
