import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { URL_SWAPI } from "../../config/config.services";
import { Observable } from "rxjs/Observable";

@Injectable()
export class StarshipsProvider {
  page: number = 1;
  starships: any[] = [];
  count: number = -1;

  constructor(public http: HttpClient) {
    console.log("Hello StarshipsProvider Provider");
    this.starships = [];
    this.count = -1;
    this.page = 1;
    this.getAllStarships();
  }

  getAllStarships() {
    let promise = new Promise((resolve, reject) => {
      if (this.starships.length == this.count) {
        resolve();
      } else {
        let url = URL_SWAPI + "starships/?page=" + this.page;
        this.http
          .get(url)
          .map(response => response)
          .subscribe(
            data => {
              console.log(data);
              if (data["error"]) {
                console.error(data["error"]);
              } else {
                this.count = data["count"];
                this.starships.push(...data["results"]);
                this.page += 1;
                resolve(this.starships);
              }
            },
            e => {
              console.error(e);
            }
          );
      }
    });

    return promise;
  }
}
