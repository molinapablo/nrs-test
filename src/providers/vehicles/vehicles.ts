import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import { Observable } from "rxjs/Observable";
import { URL_SWAPI } from "../../config/config.services";

@Injectable()
export class VehiclesProvider {
  page: number = 1;
  vehicles: any[] = [];
  count: number = -1;

  constructor(public http: HttpClient) {
    
    console.log("Hello VehiclesProvider Provider");

    this.vehicles=[];
    this.count=-1;
    this.page=1;
    this.getAllVehicles();
  }

  getAllVehicles() {

    let promise = new Promise((resolve, reject) => {

        if (this.vehicles.length == this.count) {
          resolve();
        } else {
          let url = URL_SWAPI + "vehicles/?page=" + this.page;
          console.log(url);
          this.http
            .get(url)
            .map(response => response)
            .subscribe(
              data => {
                console.log(data);

                if (data["error"]) {
                } else {
                  this.count = data["count"];
                  this.vehicles.push(...data["results"]);
                  this.page += 1;
                  resolve(this.vehicles);
                }

                //resolve();
              },
              e => {
                console.error(e);
              }
            );
        }

    });

    return promise;
  }
}
