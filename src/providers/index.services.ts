export { PeopleProvider } from './people/people';
export { VehiclesProvider } from './vehicles/vehicles';
export { FilmsProvider } from './films/films';
export { StarshipsProvider } from './starships/starships';
