import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

@Injectable()
export class PeopleProvider {

  people: People[] = [];

  constructor(public http: HttpClient) {
    console.log("Hello PeopleProvider Provider");
  }

  getPeopleByFilm(urls: any[]) {

    console.log(urls);

    this.people = [];

    let promise = new Promise((resolve, reject) => {
      for (let url of urls) {
        this.http
          .get(url)
          .map(response => response)
          .subscribe(data => {
            if (data['error']) {
            } else {
              this.people.unshift(data as People);
            }
          },e => {console.error(e);});
      }
      
      resolve(this.people);

    });

    return promise;
  }
}

export interface People {
  name: string;
  height: number;
  mass: number;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
}
