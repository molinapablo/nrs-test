import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

// Pages
import { FilmsPage, StarshipsPage, VehiclesPage, PeoplePage, PeopleDetailPage } from '../pages/index.pages'


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Providers
import { FilmsProvider, StarshipsProvider, VehiclesProvider, PeopleProvider } from '../providers/index.services';


@NgModule({
  declarations: [
    MyApp,
    FilmsPage,
    StarshipsPage,
    VehiclesPage,
    PeoplePage,
    PeopleDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FilmsPage,
    StarshipsPage,
    VehiclesPage,
    PeoplePage,
    PeopleDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FilmsProvider,
    StarshipsProvider,
    VehiclesProvider,
    PeopleProvider
  ]
})
export class AppModule {}
